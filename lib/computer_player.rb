class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def register_secret_length

  end

  def guess

  end

  def check_guess(ch)
    return [] unless @secret_word.include?(ch)
    indices = []
    @secret_word.chars.each_index {|idx| indices << idx if @secret_word[idx]==ch}
    indices
  end

  def handle_response

  end

end
