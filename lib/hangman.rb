# require "human_player"
# require "computer_player"

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess(@board)
    positions = @referee.check_guess(guess)
    self.update_board(guess,positions) unless positions.empty?
    @guesser.handle_response(guess,positions)
  end

  def update_board(guess,positions)
    positions.each {|idx| @board[idx]=guess}
  end

  def display_board
    print "\nSecret Word: "
    @board.each do |el|
      print "_" if el.nil?
      print "#{el}"
    end
    print "\n"
  end

  def play_game
    self.setup
    until @board.none?(&:nil?)
      self.display_board
      self.take_turn
      print "\e[H\e[2J"
    end
    self.display_board
  end

  def self.human_guesser_game
    dictionary = File.readlines("dictionary.txt")
    comp = ComputerPlayer.new(dictionary)
    human = HumanPlayer.new
    game = Hangman.new({guesser: human, referee: comp})
    game.play_game
  end

  def self.computer_guesser_game
    dictionary = File.readlines("dictionary.txt")
    dictionary.map! {|word| word.chomp}
    comp = ComputerPlayer.new(dictionary)
    human = HumanPlayer.new
    game = Hangman.new({guesser: comp, referee: human})
    game.play_game
  end

end

class HumanPlayer

    def initialize
      @alphabet = ("a".."z").to_a
    end

    def pick_secret_word
      puts "Enter length of secret word"
      gets.chomp.to_i
    end

    def register_secret_length(length)
      @secret_length = length
    end

    def guess(board)
      print "\nRemaining Letters : #{@alphabet}\n"
      print "\nEnter Guess: "
      guess = gets.chomp
      @alphabet.delete(guess)
      guess
    end

    def check_guess(guess)
      puts "Computer guessed : #{guess}"
      print "Is #{guess} included in the secret word ? (y/n) : "
      answer = gets.chomp
      indices = []
      until answer.downcase == "n"
        print "Enter position of #{guess} : "
        indices << gets.chomp.to_i
        print "\nMore positions? : (y/n)"
        answer = gets.chomp
      end
      indices
    end

    def handle_response(guess,positions)
      print "\n Found #{guess} at positions #{positions}"
    end

end

class ComputerPlayer

  attr_reader :alphabet, :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample.chomp
    @secret_word.length
  end

  def register_secret_length(length)
    @secret_length = length
    @candidate_words = @dictionary.select {|word| word.length==@secret_length}
    self.update_alphabet
  end

  def guess(board)
    unique_letters = board.dup
    unique_letters.delete(nil)
    unless unique_letters.empty?
      unique_letters.uniq.each {|letter| @alphabet.delete(letter)}
    end
    @alphabet.sort_by {|k,v| v}.pop.shift
  end

  def check_guess(ch)
    return [] unless @secret_word.include?(ch)
    indices = []
    @secret_word.chars.each_index {|idx| indices << idx if @secret_word[idx]==ch}
    indices
  end

  def handle_response(guess,positions)
    @candidate_words = @candidate_words.select do |word|
      (0...@secret_length).all? do |idx|
        if positions.include?(idx)
          word[idx] == guess
        else
          word[idx] != guess
        end
      end
    end
    self.update_alphabet
  end

  def update_alphabet
    @alphabet = Hash.new(0)
    @candidate_words.each do |word|
      word.each_char {|ch| @alphabet[ch]+=1}
    end
  end

end
